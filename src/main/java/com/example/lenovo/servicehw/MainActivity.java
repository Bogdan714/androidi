package com.example.lenovo.servicehw;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.panel)
    View panel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        final Handler handler = new Handler(new Handler.Callback() {
            @SuppressLint("Range")
            @Override
            public boolean handleMessage(Message msg) {
                double k = 0.5;
                if (!Double.isNaN((double) msg.obj)) {
                    k *= (double) msg.obj;
                } else {
                    k = 1;
                }
                double red = 255 / k;
                red = red > 255 ? 255 : red;
                double green = 255 - red;
                panel.setBackgroundColor(Color.rgb((int) red, (int) green, 0));
                return false;
            }
        });

        new Thread(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(MainActivity.this, MoveListenerService.class);
                startService(intent);

                while (true) {
                    Message message = new Message();
                    message.obj = Analyzer.getAverageMovement();
                    handler.sendMessage(message);
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }) {
        }.start();
    }

}
