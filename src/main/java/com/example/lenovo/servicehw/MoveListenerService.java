package com.example.lenovo.servicehw;

import android.app.IntentService;
import android.app.Service;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class MoveListenerService extends IntentService {
    public long sleepTime = 300000;
    public long activeTime = 6000;

    private List<Point> measureTempStore;
    public static Analyzer analyzer;

    public MoveListenerService() {
        super("MoveListenerService");
        Log.d("myLog", "MoveListenerService constructor build");
    }

    SensorEventListener listener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {
            if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
                double x, y, z;
                x = event.values[0];
                y = event.values[1];
                z = event.values[2];
                measureTempStore.add(new Point(x, y, z));

            }

        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }
    };

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        measureTempStore = new ArrayList<>();
        while (true) {
            SensorManager sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
            sensorManager.registerListener(listener, sensorManager
                    .getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
            try {
                Thread.sleep(activeTime);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            sensorManager.unregisterListener(listener);
            analyzer.analyze(measureTempStore);
            measureTempStore.clear();

            try {
                Thread.sleep(sleepTime);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
