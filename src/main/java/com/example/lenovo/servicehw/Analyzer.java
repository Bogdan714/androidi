package com.example.lenovo.servicehw;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public final class Analyzer {
    private static List<Point> measures = new ArrayList<>();

    synchronized public static void analyze(List<Point> forMeasure) {
        Point min, max;
        min = new Point(forMeasure.get(0).x, forMeasure.get(0).y, forMeasure.get(0).z);
        max = new Point(forMeasure.get(0).x, forMeasure.get(0).y, forMeasure.get(0).z);
        for (Point point : forMeasure) {
            min.x = Math.min(min.x, point.x);
            min.y = Math.min(min.y, point.y);
            min.z = Math.min(min.z, point.z);

            max.x = Math.max(max.x, point.x);
            max.y = Math.max(max.y, point.y);
            max.z = Math.max(max.z, point.z);
        }
        double dx = Math.abs(max.x - min.x);
        double dy = Math.abs(max.y - min.y);
        double dz = Math.abs(max.z - min.z);

        measures.add(new Point(dx, dy, dz));
        if (measures.size() > 500) {
            measures.remove(0);
        }
    }

    public static List<Point> getMeasures() {
        return measures;
    }

    synchronized public static double getAverageMovement() {
        double result = 0;
        for (Point measure : measures) {
            result += (measure.x + measure.y + measure.z) / 3.0;
        }
        result /= measures.size();
        return result;
    }
}
